\section{Discussion}
\label{s:discussion}

\subsection{Unexpected results}
Whereas the impact of error/cost tradeoff on prediction accuracy that I found
was expected, I was surprised by the magnitude of the reduction in feature
cost. As can be seen in \cref{f:totalcost}, the use of features only shrinks
approximately $1\%$ in the largest dataset and even less in the smaller sets. I
suspect this has to do with either my implementation, the datasets used or a
mixture of both factors.

\paragraph{Implementation} The main problem area is the caching of acquired
features. Nan et al.~\cite{nan2016pruning} assume that acquisition cost for a
single feature is only incurred once; I implemented this for each tree
individually, but I now suspect that caching has to be applied for the whole
ensemble. Individual tree caching does not have a large impact unless the trees
are fairly large and a single feature is acquired many times, whereas in
hindsight global caching makes more sense to judge feature cost over an
ensemble.

To illustrate, imagine a sample $s$ made up of $4$ features. Now consider a
tree that has multiple splits on the same feature. In my implementation, only
the first split that $s$ encounters for a certain feature incurs a cost; the
subsequent checks on that same feature do not incur additional cost in that
tree.  However, this process repeats itself for the next tree that $s$ runs
through. If every tree in the forest looks at the first feature in $s$, the
total cost would add up to the feature cost times the number of trees in the
forest. Global caching would mean that the acquisition cost is only incurred
the first time $s$ encounters a feature in \emph{any} tree, and all other times
that feature is used do not cause additional cost.

This difference in caching mechanism results in different network problems,
which in turn results in a different optimal solution to \cref{e:primaldual}.
It would explain why I did not find the performance increase claimed by Nan et
al.~\cite{nan2016pruning}.

\paragraph{Datasets used} As mentioned in \cref{s:experiment}, I used
relatively small datasets for my tests in order to deal with slow code. The
tiny number of features may have had an impact in not being able to reduce
feature acquisition drastically, simply due to the fact that there were no
features that could be `disregarded' without compromising prediction accuracy.
However, it should be noted that the large decrease in prediction accuracy
coupled with relatively minimal change in feature acquisition points to the
problem lying in the implementation.

\subsection{Future research \& improvements}
% fixing caching
% gradient descent
% justify Spark
% improving on algorithm resource use
Due to various problems along the way, this project has not covered everything
it set out to do. There are some topics worthy of further investigation that I
will discuss here.

\paragraph{Correct caching} The first improvement on my work is to fix the
caching problem highlighted in \cref{s:discussion}. Unfortunately, I did not
have the time to fix this issue myself; assuming that it is indeed the problem
causing my findings, fixing it should improve pruned models to match up to the
results found by Nan et al.~\cite{nan2016pruning}.

\paragraph{Optimize code} As mentioned in \cref{s:implementation}, I wrote all
my code from scratch. This resulted in a severely impaired runtime compared to
the runtimes that Nan et al.~\cite{nan2016pruning} reported (minutes versus
hours with my implementation). Their code uses optimized libraries such as
IBM's CPLEX~\cite{cplex2009v12} to solve the problem, and there is no way to
compete with that unfortunately. I made everything from scratch as there is no
package similar to CPLEX for Scala/Spark, however it is very likely that those
packages will exist in the future and can be used to improve upon my work.

\paragraph{Prediction rule} As mentioned in \cref{s:experiment}, I did not find
any differences in prediction error using two different prediction rules on the
Iris dataset. This is probably due to the small number of features and the
relatively `easy' dataset; maybe on a set with more features, the custom rule
would make for a significant improvement. Unfortunately, there was no time to
test this further.

\paragraph{Gradient descent for dual problem stepsize} The current
implementation uses a constant stepsize value for the dual variable update; see
\cref{s:ensembleoptimization}. Using a static value means that even though the
difference between the dual and primal solutions is very large, the steps
towards convergence have the same size. Thus, it may take way longer than
needed to reach a solution when the dual and primal values start very far
apart. Using gradient descent, you could project a suitable stepsize for each
iteration based on the duality gap for that iteration, making the solutions
converge faster. This means that the running time could be drastically
decreased, especially for large datasets where the duality gap starts out with
a gigantic value.

\paragraph{Explore Spark-specific properties} The original intent of this
project was to explore the scalability of BudgetPrune in a distributed
framework like Spark.  Unfortunately, I never got to this part. It would be
interesting to see how BudgetPrune could be adapted to deal with distributed
data, and how it could perform with a pipeline-suitable implementation.

\paragraph{Improve on memory requirements} BudgetPrune is quite heavy on memory
use. Consider a dataset with $S$ samples and $k$ features per sample, used by
an ensemble of $T$ trees; worst case, we store $2 \times T \times S \times
k_{max}$ values ($w_{k,i}^{(t)}$ and $\beta_{k,i}^{(t)}$) plus another $S
\times k$ for the primal variables $w_{k,i}$. For large datasets, this can
really be a problem.  Intuitively, it should be able to alter the algorithm
such that we do not have to keep track of values for every feature of every
sample. This would be a major improvement since it allows for better scaling to
huge datasets that you would typically use in an environment like Spark.
