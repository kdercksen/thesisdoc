\section{Implementation}
\label{s:implementation}

The implementation in Nan et al.~\cite{nan2016pruning} uses IBM's CPLEX
\cite{cplex2009v12} solver for the primal update step, whereas I wrote all the
code from parsing the Spark RFs to prediction with the pruned RFs myself. At
the time of writing the code, there was no open source code for BudgetPrune
released by Nan et al., however it was released recently~\cite{nan2017code}.

\subsection{Baseline model}
The model that I started out with is an Apache Spark RF classifier. I decided
on using the RF classifier from the ML API, since the old MLlib API will be
deprecated in the future. The process is basically as follows:

\begin{lstlisting}[style=myScalaStyle, caption=Train a basic RF classifier.]
// Load libsvm data from file
val data = spark.read.format("libsvm").load("path/to/data.txt")

// Split data into train/test chunks
val Array(train, test) = data.randomSplit(Array(0.7, 0.3))

// Train a random forest classifier on the training split
val rf = new RandomForestClassifier()
    .setLabelCol("label")
    .setFeaturesCol("features")
    .setImpurity("gini")
    .setFeatureSubsetStrategy("auto")
    .setMaxDepth(5)
    .setMaxBins(32)
    .setNumTrees(20)
    .fit(train)

// Predict classes for the test split
val predictions = rf.transform(test)

// Create an evaluator to calculate the prediction accuracy
val ev = new MulticlassClassificationEvaluator()
    .setLabelCol("label")
    .setPredictionCol("prediction")
    .setMetricName("accuracy")

println("Prediction accuracy: " + ev.evaluate(predictions))

// Save model to file for reuse
rf.save("path/to/model")
\end{lstlisting}

The values listed here are the ones that I used for each of the base models. In
principle one could use any random forest as input for the pruning algorithm,
so I did not try to optimize anything about the values used; I did however keep
them the same across the different base models that I trained in order to get
consistent results come time to compare to the pruned models.

\subsection{BudgetPrune in Spark}
Implementing the pruning algorithm was the meat of the thesis. I encountered
quite a few issues along the way, and I will detail my work in this section.

\subsubsection{Parsing Spark RFs}
The ML API is a highlevel abstraction meant to be easy to use, but this means
that the core data of the model is hard to get to. When saving a RF to file, it
is serialized in a format that can be loaded as a \emph{DataFrame}; see the
ML documentation~\cite{sparkdocs} for more details.

First, I defined some datatypes to hold the RF structure.

\begin{lstlisting}[style=myScalaStyle, caption=Data structures to hold Spark RF]
case class PruneTree(
  // Different ID for every tree in the RF
  treeID: Int,
  // List of nodes in this tree
  nodes: Array[PruneNode]
)

case class PruneNode(
  id: Int,
  // The gini coefficient calculated at this node
  impurity: Double,
  // Class to predict if this node were a leaf node
  prediction: Double,
  // Count per class of all the examples coming through this node
  impurityStats: Array[Double],
  // Information gain at this node
  gain: Double,
  // ID of left/right child
  leftChild: Int,
  rightChild: Int,
  // Feature split information
  split: PruneSplit
)

case class PruneSplit(
  // Feature that is used to split on in this node
  featureIndex: Int,
  // When this feature is categorical, the array holds a list of values
  // that go down the left subtree. When the feature is continuous,
  // array[0] holds the threshold value: f <= threshold goes down the
  // left subtree
  leftCategoriesOrThreshold: Array[Double],
  // -1 if the feature is continuous, else holds the number of
  // categories for this feature
  numCategories: Int
)

// Define a RF as a list of trees
type PruneRF = Array[PruneTree]
\end{lstlisting}

These datatypes copy the internal structure of the Spark datatypes to make it
easy to parse the models from file. The actual parsing code can be found in the
complete implementation ({\tt TreeIO.scala}).

\subsubsection{Building the constraint matrix}
I approached this preparatory part of the algorithm in two main steps.
\begin{itemize}
    \item Create node constraints (\cref{e:justnodematrix}).
    \item Create feature constraints and stack them with the node constraints,
        preserving post-order arrangement of the rows
        (\cref{e:nodefeaturematrix}).
\end{itemize}

The node constraints are fairly straightforward to create. Simply find all
possible root to leaf paths in the tree; then for each path, create a row in
the constraint matrix, setting the node variables in the path to $1$ and the
rest to zero.

\begin{lstlisting}[style=myScalaStyle, caption=Find all paths in the tree.]
def findAllPaths(tree: PruneTree): Seq[PruneNode] = {
  val leafs = tree.nodes.filter(_.leftChild == -1)
  for (leaf <- leafs) yield ShortestPath.pathToNode(leaf.id, tree)
}
\end{lstlisting}

We can then build the first part of the matrix by iterating over all the paths,
creating rows as previously mentioned. The next step is to create the feature
constraints.  This is a little more involved and requires some additional
preparatory work. For every sample in the dataset, we need to work out which
features it uses when running through a tree (the set of features will thus be
different per individual tree). We also need to keep in mind that BudgetPrune
assumes that a feature, once acquired, is cached in memory and does not rack up
additional acquisition cost when it is inspected more than once. I implement
this caching per tree; i.e. if a tree looks at a certain feature more than once
for a single sample, only the first time incurs an acquisition cost. There is
reason to believe that this caching should be forest-wide; see
\cref{s:discussion} for more information.

To accomplish this I wrote a slightly altered prediction function that, given a
{\tt Prunetree} and a sample, returns the unique features acquired in order to
route the sample to a leaf node on the given tree (see {\tt
TransformTree.scala}). This also includes the tree nodes that were visited to
get to the feature: for example, on row 5 of \cref{e:nodefeaturematrix} we see
that for sample $1$ using feature $1$ results in $w_{1,1} = 1$ as well as $z_0
= z_1 = 1$.  Once we have this data, the code to create the additional matrix
rows is almost identical to the code for the node constraints, with the
exception of also setting $w_{k,i}$ variables to $1$.

% Q: Maybe it existed as a CoordinateMatrix?
\paragraph{Matrix datatype}
It should be noted that Scala/Spark does not supply a Matrix datatype that fit
my needs during this thesis. Datatypes such as {\tt CoordinateMatrix} or {\tt
SparseMatrix} are supplied by Spark, but do not offer functionality for adding
rows, iterating over the matrix or other utility functions that were necessary.
Dealing with extremely sparse matrices, I decided to implement my own matrix
datatype that was suitable to do row operations and sparse storage. An outline
can be found below.

\begin{lstlisting}[style=myScalaStyle, caption=Vector and Matrix traits.]
sealed trait Vector[T] extends Iterable[T] {
  // Length of vector
  def numElements: Int
  // Get element at index i
  def apply(i: Int): T
  // Invert sign of every element
  def inverted: Vector[T]
  // Get underlying sorted map structure
  def getMap: TreeMap[Int, T]
  // Get iterator to loop over vector
  def iterator: Iterator[T]
  // Addition with another vector (element-wise)
  def +(that: Vector[T]): Vector[T]
  // Subtraction with another vector (element-wise)
  def -(that: Vector[T]): Vector[T] = this + that.inverted
}

sealed trait Matrix[T] extens Iterable[(Int, Vector[T])] {
  // Number of rows in matrix
  def numRows: Int
  // Get row at index i
  def apply(i: Int): Vector[T]
  // Add a row at index i
  def addRow(i: Int, row: Vector[T]): Matrix[T]
  // Loop over rows
  def iterator: Iterator[(Int, Vector[T])]
  // Check if every column contains a single (-)1 and rest zeroes
  def isNetworkMatrix: Boolean
}
\end{lstlisting}

Based on these traits I implemented a {\tt SparseMatrix} datatype backed by a
{\tt TreeMap} (a sorted map datatype available in Scala) in an effort to save
memory and not use fullsize arrays. Especially with larger datasets, this makes
quite a difference in memory usage. For the full implementation, see {\tt
Matrices.scala}.

\subsubsection{Minimization by shortest-path algorithm}
The network matrix that results from transforming the constraint matrix rows of
a tree as shown in \cref{e:networkmatrix} can be converted into a graph. Every
row in the network matrix becomes a vertex in the graph, and the edges are
defined by the location of the $-1$ and $1$ values (see \cref{f:graph}). Every
edge has a cost associated with it; the edges with node variables use
prediction error as their cost (these costs stay constant), and the edges with
feature variables use costs computed from the dual variables (these costs thus
change after every iteration).

\begin{figure}
    \centering
    \begin{tikzpicture}[->,>=stealth', shorten >=1pt, auto,
            node distance=2cm, semithick]
        \node[initial, state] (r0) {$0$};
        \node[state] (r1) [right of=r0] {$1$};
        \node[state] (r2) [right of=r1] {$2$};
        \node[state] (r3) [right of=r2] {$3$};
        \node[state] (r4) [right of=r3] {$4$};
        \node[state, accepting] (r8) [below of=r0] {$8$};
        \node[state] (r7) [right of=r8] {$7$};
        \node[state] (r6) [right of=r7] {$6$};
        \node[state] (r5) [right of=r6] {$5$};

        \path[->] (r0) edge node {$z_0$} (r8)
        (r0) edge [bend left=50] node {$z_1$} (r3)
        (r0) edge node {$z_2$} (r1)
        (r1) edge node {$z_3$} (r2)
        (r3) edge node {$z_4$} (r6)
        (r3) edge node {$z_5$} (r4)
        (r4) edge node {$z_6$} (r5)
        (r2) edge node {$w_{1,1}^{(1)}$} (r3)
        (r6) edge node {$w_{2,1}^{(1)}$} (r7)
        (r7) edge node {$w_{2,2}^{(1)}$} (r8)
        (r5) edge node {$w_{3,2}^{(1)}$} (r6);
    \end{tikzpicture}
    \caption{Example graph for \cref{e:networkmatrix}. Note that the vertex
    numbers correspond to a row index in \cref{e:networkmatrix}; i.e.\ vertex
    $0$ corresponds to the first row $-r1$ in the matrix.}
    \label{f:graph}
\end{figure}

Due to the fact that the rows in the network matrix are sorted in a post-order
arrangement, the graph that results from the conversion is a \emph{directed
acyclic graph} (DAG)~\cite{thulasiraman19925}. Finding the shortest path in a
DAG can be done in linear time $O(N + M)$ where $N$ is the number of edges and
$M$ is the number of vertices~\cite{christofides1975algorithmic}, in contrast
to arbitrary graphs where shortest path algorithms are slower.

\paragraph{Topological ordering} DAGs can always be topologically sorted. This
means that the vertices are ordered such that for every directed arc $uv$ from
vertex $u$ to $v$, vertex $u$ comes before $v$ in the ordering. As can be
easily seen, the vertices in \cref{f:graph} are already numbered in topological
order due to the order of the matrix rows in \cref{e:networkmatrix}.

We can now find an optimal path through the DAG using the algorithm listed in
\cref{a:dagshortestpath} (pseudocode, for real implementation see {\tt
TransformTree.scala}).

\begin{algorithm}
    \caption{DAG shortest path algorithm in pseudocode.}
    \label{a:dagshortestpath}
    \SetKwInOut{Input}{input}
    \SetKwInOut{Output}{output}

    \Input{list of vertices $V$, source vertex $s$}
    \Output{array $dist$ of the same length as $V$, containing for each vertex
        $v \in V$ the shortest distance from $s$ to $v$}
    let $dist$ be an array of the same length as $V$; set $dist[s] = 0$ and all
    other $dist[v] = \infty$\;
    let $path$ be an array of the same length as $dist$, with all elements set
    to null; each $path[v]$ will hold the parent vertex in the path from $s$ to
    $v$\;
    \ForEach{vertex $u \in V$ starting from $s$}{
        \ForEach{vertex $v$ such that there exists an edge from $u$ to $v$}{
            let $w$ be the cost associated with edge $uv$\;
            \If{$dist[v] > dist[u] + w$}{
                let $dist[v] = dist[u] + w$\;
                let $path[v] = u$\;
            }
        }
    }
\end{algorithm}

We can extract the optimal path from the {\tt path} array returned in
\cref{a:dagshortestpath}. This path corresponds to a specific tree pruning. For
example, if we find the shortest path in \cref{f:graph} to be $0-3-6-7-8$ we
can see that $z_1 = z_4 = w_{2,1}^{(1)} = w_{2,2}^{(1)} = 1$; the other
variables are set to zero. This corresponds to pruning the tree in
\cref{f:tree1} at nodes $1$ and $4$, resulting in the tree seen in
\cref{f:tree2}. For another example, see also \cref{f:tree3}.

\begin{figure}
    \captionsetup[subfigure]{position=b}
    \centering
    \subcaptionbox{Tree from \cref{f:tree1} pruned based on solution
    $0-3-6-7-8$ in \cref{f:graph}.\label{f:tree2}}{%
    \begin{subfigure}[b]{.4\textwidth}
        \centering
        \begin{tikzpicture}[
            every node/.style={minimum size=4mm, inner sep=0.5mm, circle, draw}
            ]
            \node (root) {$0^2$}
                child {node {$1$}}
                child {node {$4$}};
        \end{tikzpicture}
    \end{subfigure}}\hfill
    \subcaptionbox{Tree from \cref{f:tree1} pruned based on solution
    $0-3-4-5-6-7-8$ in \cref{f:graph}.\label{f:tree3}}{%
    \begin{subfigure}[b]{.4\textwidth}
        \centering
        \begin{tikzpicture}[
            every node/.style={minimum size=4mm, inner sep=0.5mm, circle, draw},
            level 1/.style={sibling distance=20mm},
            level 2/.style={sibling distance=10mm}
            ]
            \node (root) {$0^2$}
                child {node {$1$}}
                child {
                    node {$4^3$}
                    child {node {5}}
                    child {node {6}}
                };
        \end{tikzpicture}
    \end{subfigure}}
    \caption{Examples of tree prunings based on shortest-path solutions.}
    \label{f:prunedtrees}
\end{figure}

\newpage
\subsubsection{Ensemble optimization}
\label{s:ensembleoptimization}
Optimizing each tree gives us a list of plausible solutions that we can use to
find a more accurate lowerbound solution to the primal problem. The (slightly
simplified) code of the full algorithm looks like this:

\begin{lstlisting}[style=myScalaStyle, caption={For the full implementation,
refer to {\tt TransformTree.scala}}]
// Initialize dual variables to zero
var duals = Array.ofDim[Double](numTrees, numSamples, numFeatures)

// Create the network problems with some default feature costs
var solutions = findSolutions(ensemble, defaultCosts)

// Initialize duality gap to something big
var dualityGap = 1e8

// Alternate between primal/dual updates until converged or max
// iterations is reached
while (i < maxIterations || dualityGap < epsilon) {
    // Calculate primal variables
    val primals = updatePrimalVars(solutions, lambda, duals)
    // Find new solution to networks with updated primal variables
    solutions = findSolutions(ensemble, primals)
    // Update dual variables based on new primal solution
    duals = updateDualVars(primals, duals, solutions, stepsize)
    // Calculate duality gap based on difference in primal/dual
    // variables
    dualityGap = calculateGap(primals, duals)

    i += 1
}
\end{lstlisting}

The {\tt findSolutions} function is parallelized using Scala's builtin {\tt
Future} framework; see \cref{l:futures}.

\begin{lstlisting}[style=myScalaStyle, caption=Illustrating parallelization., label={l:futures}]
def findSolutions(ensemble, primals): Seq[Solution] = {
    // Distribute tree jobs over all available cores
    solutions = for (tree <- ensemble) yield {
        Future { solveForTree(tree, primals) }
    }
    // Blocking execution until all computations are done.
    // The timeout is infinite because the function is guaranteed to
    // return eventually, and we cannot continue the algorithm if
    // it would not.
    val futureSolutions = Future.sequence(solutions)
    Await.result(futureSolutions, Duration.Inf)
}
\end{lstlisting}

\subsubsection{Alternate prediction rule}
Spark's RF classifier by default uses a majority vote to classify unseen
samples (a sample is routed through every tree and the class label that was
predicted most often is the RFs classification for that particular sample).
Depending on the tree depth, leaf nodes in trees that are not pruned are often
pure or very close to pure (meaning that each leaf contains only one class of
samples). When pruning the tree, internal nodes turn into leaves with mixed
classes, making a simple majority vote less reliable. This is illustrated in
\cref{f:voteexample}.

\begin{figure}
    \captionsetup[subfigure]{position=b}
    \centering
    \subcaptionbox{Pure leaf nodes. 70\% of samples have class $2$. Both leaf
        nodes classify all samples routed through them 100\%
        correctly.\label{f:pureleaves}}{%
    \begin{subfigure}[b]{.4\textwidth}
        \centering
        \begin{tikzpicture}[
            every node/.style={minimum size=4mm, inner sep=0.5mm, circle, draw}
            ]
            \node (root) {$0_{<=.3,>.7}^2$}
                child {node {$1_{1.0}^1$}}
                child {node {$2_{1.0}^2$}};
        \end{tikzpicture}
    \end{subfigure}}\hfill
    \subcaptionbox{Impure collapsed node resulting from \cref{f:pureleaves}.
        This node now only classifies 70\% of samples correctly.\label{f:impureleaf}}{%
    \begin{subfigure}[b]{.4\textwidth}
        \centering
        \begin{tikzpicture}[
            every node/.style={minimum size=4mm, inner sep=0.5mm, circle, draw},
            level 1/.style={sibling distance=20mm},
            level 2/.style={sibling distance=10mm}
            ]
            \node (root) {$0_{0.7}^2$};
        \end{tikzpicture}
    \end{subfigure}}
    \caption{Disadvantage of using majority voting with pruned trees. Predicted
    class at a node is in superscript, the accuracy of that prediction in
    subscript.}
    \label{f:voteexample}
\end{figure}

Nan et al.~\cite{nan2016pruning} suggest the following prediction rule:

\begin{align}
    p = \frac{1}{T} \sum_{t=1}^T p_t
    \label{e:predictionrule}
\end{align}

For each sample $x$, we acquire the probability distribution over label classes
$p_t$ at the leaf node that $x$ is routed to. Then we aggregate those classes
with \cref{e:predictionrule} and return the class with the highest probability
as prediction for $x$. This means that, in the case of \cref{f:impureleaf}, we
now know that the prediction is only 70\% certain. If some other prediction
ends up with a higher probability after running a sample through the whole
ensemble, we can thus avoid some prediction errors potentially introduced by
majority voting. Nan et al.~\cite{nan2016pruning} claim that their prediction
rule consistently gives a lower prediction error than majority voting with
pruned trees.

The code for this is relatively straightforward:

\begin{lstlisting}[style=myScalaStyle, caption=Alternate prediction rule.]
// Simplified version of prediction function
def predict(forest: IndexedSeq[PruneTree], sample: Array[Double]): Double = {
    // Get a list with class probability distributions for each
    // tree in the forest
    val predictions = for (t <- forest) yield t.predict(sample)
    val summed = predictions.transpose.map(_.sum)
    // Return the class associated with the highest probability
    summed.zipWithIndex.maxBy(_._1)._2
}
\end{lstlisting}
