\section{Theory}
The research by Nan et al.~\cite{nan2016pruning} that my work is based on
considers solving the Lagrangian relaxed problem of finding an optimal model
considering prediction-time resource constraints, also known as the error-cost
tradeoff problem:

\begin{align}
    \underset{f \in \mathcal{F}}{\min} E_{(x,y)\sim\mathcal{P}} [e(y, f(x))] + \lambda E_{x\sim\mathcal{P}_x} [C(f, x)]
    \label{e:lagrangian}
\end{align}

where sample/label pairs $(x, y)$ are drawn from a distribution $\mathcal{P}$,
$e(y, \hat{y})$ is the error function ($\hat{y}$ is the predicted label, $y$ is
the actual label), $C(f, x)$ is the cost of running sample $x$ through
classifier $f$ and $\lambda$ is the tradeoff parameter (i.e.\ how heavy will
feature cost influence the final solution). In the case of this thesis,
$\mathcal{F}$ is the space of possible random forest (RF) classifiers. Each RF
is made up of trees $\mathcal{T}_0$, \ldots, $\mathcal{T}_n$.

\subsection{Lagrangian dual problem}
The method of Lagrange multipliers is a strategy for optimizing a function subject to equality constraints. As an example, imagine the following problem:

\begin{align}
    \max f(x, y) \text{ s.t. } g(x, y) = c
\end{align}

We can introduce a new variable $\lambda$ called a \emph{Lagrange multiplier} and define a new function:

\begin{align}
    \mathcal{L}(x, y, \lambda) = f(x, y) - \lambda (g(x, y) - c)
\end{align}

We can then find a $\lambda$ such that we minimize $\mathcal{L}$.

The duality principle is that we can view an optimization problem from two
perspectives; the \emph{primal} and \emph{dual} problem. A solution to the dual
problem gives a lower bound to the solution of the primal problem. The two
solutions do not have to be equal; the difference between them is called the
duality gap, and an optimization algorithm will typically stop when this gap is
small enough.

\subsection{Formal definition of pruning}
I will be using the notation defined in Section 3 of Nan et
al.~\cite{nan2016pruning}. In order to formulate random forest pruning in terms
of \cref{e:lagrangian}, we will define pruning as a 0-1 integer problem. Given
a decision tree $\mathcal{T}$, we will index its nodes as $h \in
\{1,\ldots,|\mathcal{T}|\}$.

First, introduce the binary variable $z$ for each node $h$ in tree
$\mathcal{T}$:

\begin{align}
    z_h =
    \begin{cases}
        1 & \text{if node $h$ is a leaf in the pruned tree} \\
        0 & \text{otherwise}
    \end{cases}
    \label{e:nodevar}
\end{align}

Let $p(h)$ be the set of nodes that lie on the path from the root to and
including $h$.  Consider that every pruned tree $\mathcal{T}^p$ should be a
subset of the original tree $\mathcal{T}$; thus, all valid pruned trees satisfy
the constraints:
\begin{align}
    \sum_{u \in p(h)} z_u = 1
    \label{e:pathconstraint}
\end{align}

where $h$ is any leaf node in the tree. Take for example the tree in
\cref{f:tree1}; $p(2)$ would be made up of $\{z_0, z_1, z_2\}$. The constraint
specified in \cref{e:pathconstraint} holds for this combination of nodes since
only $z_2 = 1$ because it is a leaf node. We can repeat this for every leaf
node in the tree and the constraint will hold; hence, this tree is valid. Were
we to prune the tree at node $1$, $z_0 + z_1 = 1$ holds and thus this pruned
tree is valid, as well as a subset of the original tree ($\{0,1,4,5,6\} \subset
\{0,1,2,3,4,5,6\}$).

We can define the expected error as the number of misclassified training
samples in the leaf nodes:

\begin{align*}
    e_h = \sum_{i \in S_h} \mathds{1}_{[y^{(i)} \neq Pred_h]}
\end{align*}

where $S_h$ is the subset of total samples that is routed through node $h$ and
$Pred_h$ is the predicted label at $h$. To illustrate, imagine that we run
$100$ samples through the tree in \cref{f:tree1} and the first $50$ of those
samples end up going down the left subtree.  At node $0$ we have $S_0 =
\{s_1,\ldots,s_{100}\}$. At its child nodes we would have $S_1 =
\{s_1,\ldots,s_{50}\}$, and $S_4 = \{s_{51},\ldots,s_{100}\}$.

Lastly, given the binary variables:

\begin{align*}
    w_{k,i} =
    \begin{cases}
        1 & \text{if feature $k$ is used by sample $i$ in any tree $\mathcal{T}$} \\
        0 & \text{otherwise}
    \end{cases}
\end{align*}

we define the expected feature cost of an example as $\frac{1}{N}
\sum_{i=1}^{N} \sum_{k=1}^{K}w_{k,i}$ (we always assume a feature cost of
$1.0$).

Using these definitions, we can combine the pruning constraints, error and
costs into an integer program based on the way the variables are related to
each other.

\begin{align}
    \underset{z_h^{(t)},w_{k,i}^{(t)},w_{k,i} \in [0,1]}{\min}
    \overbrace{\frac{1}{NT} \sum_{t=1}^T \sum_{h \in \mathcal{T}_t} e_h^{(t)} z_h^{(t)}}^\text{error} + \lambda
    \left(
        \overbrace{\frac{1}{N} \sum_{k=1}^K \sum_{i=1}^N w_{k,i}}^\text{feature acquisition cost}
    \right)
    \label{e:ip}
\end{align}

For additional details on variables and their meaning that may not be defined
in this paper, refer back to Nan et al.~\cite{nan2016pruning}.

\subsubsection{LP relaxation}
Nan et al.~\cite{nan2016pruning} show that \cref{e:ip} can be relaxed to a
linear programming problem given Lemma $3.1$ in their paper.  I will provide an
example of the transformation of constraints into a network matrix.  A network
matrix has a single $-1$ and a single $1$ in each column, the rest of the
values in that column being zero.  First we define the rows for the sum
constraint on the node variable defined in \cref{e:nodevar}:

\begin{align}
    \kbordermatrix{
        & z_0 & z_1 & z_2 & z_3 & z_4 & z_5 & z_6 \\
        r_1 & 1 & 1 & 1 & 0 & 0 & 0 & 0 \\
        r_2 & 1 & 1 & 0 & 1 & 0 & 0 & 0 \\
        r_3 & 1 & 0 & 0 & 0 & 1 & 1 & 0 \\
        r_4 & 1 & 0 & 0 & 0 & 1 & 0 & 1 \\
    }
    \label{e:justnodematrix}
\end{align}

\begin{figure}
    \centering
    \begin{tikzpicture}[
        every node/.style={minimum size=4mm, inner sep=0.5mm, circle, draw},
        level 1/.style={sibling distance=20mm},
        level 2/.style={sibling distance=10mm}
        ]
        \node [label={[yshift=-.6cm]above:$\theta = (2, 0.7)$}] (root) {$0^2$}
            child {
                node [label={left:$\theta = (1, 0.3)$}] {$1^1$}
                child {node [label={below:$r_1$}] {2}}
                child {node [label={below:$r_2$}] {3}}
            }
            child {
                node [label={right:$\theta = (3, 0.2)$}] {$4^3$}
                child {node [label={below:$r_3$}] {5}}
                child {node [label={below:$r_4$}] {6}}
            };
    \end{tikzpicture}
    \caption{Example of tree with 7 nodes and 3 feature splits. The $r_n$ below
    the leaf nodes correspond to rows in \cref{e:justnodematrix}. $\theta (f,
    t)$ routes samples with feature $f \leq t$ to the left subtree.}
    \label{f:tree1}
\end{figure}

Each row in \cref{e:justnodematrix} defines \cref{e:pathconstraint} on a
possible path through the tree displayed in \cref{f:tree1}; for example, if we
take the path from the root node to leaf node $2$, this is translated to the
constraint $z_0 + z_1 + z_2 = 1$. The second step is to expand this matrix to
include the expanded constraint as defined in Lemma 3.1 of Nan et
al.~\cite{nan2016pruning}, where each feature split is considered a type of
extra child node:

\begin{align}
    \kbordermatrix{
        & z_0 & z_1 & z_2 & z_3 & z_4 & z_5 & z_6 & w_{1,1}^{(1)} & w_{2,1}^{(1)} & w_{2,2}^{(1)} & w_{3,2}^{(1)} \\
        r_1 & 1 & 1 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
        r_2 & 1 & 1 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
        fr_1 & 1 & 1 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 \\
        r_3 & 1 & 0 & 0 & 0 & 1 & 1 & 0 & 0 & 0 & 0 & 0 \\
        r_4 & 1 & 0 & 0 & 0 & 1 & 0 & 1 & 0 & 0 & 0 & 0 \\
        fr_4 & 1 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 1 \\
        fr_2 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 \\
        fr_3 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 \\
    }
    \label{e:nodefeaturematrix}
\end{align}

The extra four rows $fr_n$ in \cref{e:nodefeaturematrix} define the feature
acquisition constraints for two examples, example $1$ being routed to leaf node
$2$ and example $2$ to leaf node $5$. For example, $s_1 = (.2, .5, 0)$ and $s_2
= (0, .8, .1)$ would result in the proposed routings.

Now, assuming that the rows are ordered so that the leaf nodes are in a
post-order fashion (in the case of \cref{f:tree1}, post-order would be
2-3-1-5-6-4-0), we can transform the matrix in \cref{e:nodefeaturematrix} into
an equivalent network matrix \cref{e:networkmatrix} through row operations:

\begin{align}
    \kbordermatrix{
        & z_0 & z_1 & z_2 & z_3 & z_4 & z_5 & z_6 & w_{1,1}^{(1)} & w_{2,1}^{(1)} & w_{2,2}^{(1)} & w_{3,2}^{(1)} \\
        -r_1 & -1 & -1 & -1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
        r_1 - r_2 & 0 & 0 & 1 & -1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
        r_2 - fr_1 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & -1 & 0 & 0 & 0 \\
        fr_1 - r_3 & 0 & 1 & 0 & 0 & -1 & -1 & 0 & 1 & 0 & 0 & 0 \\
        r_3 - r_4 & 0 & 0 & 0 & 0 & 0 & 1 & -1 & 0 & 0 & 0 & 0 \\
        r_4 - fr_4 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & -1 \\
        fr_4 - fr_2 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & -1 & 0 & 1 \\
        fr_2 - fr_3 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & -1 & 0 \\
        fr_3 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 \\
    }
    \label{e:networkmatrix}
\end{align}

You could extend this matrix to hold a complete RF, but the desired property of
this approach is that it is now possible to solve the systems that these
matrices represent for each tree individually. The network matrices can be
converted to shortest path problems and solved easily in parallel (see
\cref{f:graph}).

\subsection{Primal-dual approach}
The problem defined in \cref{e:ip} is NP-hard to solve, and even though its LP
relaxation is solvable in polynomial time it can still be way too large to
solve within a feasible timeframe. The complexity comes down to $O(T \times
|T_{max}| + N \times T \times K_{max})$~\cite{nan2016pruning} where $T$ is the
number of trees in the RF, $|T_{max}|$ is the maximum number of nodes in a
tree, $N$ is the number of samples in the used dataset and $K_{max}$ is the
maximum number of features a sample uses within a tree. In other words, we are
looking at $O(T^3)$ scaling, which is really bad when our ensembles become
large. The primal-dual approach proposed by Nan et al.~\cite{nan2016pruning}
decomposes the optimization into many subproblems that can be parallelized;
each one is then solved as a shortest path problem. This improves the runtime
complexity to $O(\frac{T}{p}(|T_{max}| + N \times K_{max})\log(|T_{max}| + N
\times K_{max}))$ where $p$ is the number of processors available. Now the
complexity scales only with $\frac{T}{p}$ and we can use much larger ensembles.

To accomplish this, Nan et al.~\cite{nan2016pruning} improve the problem of
\cref{e:ip} into a primal-dual problem:

\begin{align}
    \underset{\beta_{k,i}^{(t)} \geq 0}{\max}
    \underset{\substack{z_h^{(t)} \in [0,1] \\ w_{k,i}^{(t)} \in [0,1] \\ w_{k,i} \in [0, 1]}}{\min}
    \frac{1}{NT} \sum_{t=1}^T \sum_{h \in \mathcal{T}_t} \hat{e}_h^{(t)} z_h^{(t)} +
    \lambda \left( \frac{1}{N} \sum_{k=1}^K \sum_{i=1}^N w_{k,i} \right) \nonumber \\
    + \sum_{t=1}^T \sum_{i=1}^N \sum_{k \in K_{t,i}} \beta_{k,i}^{(t)} ( w_{k,i}^{(t)} - w_{k,i} )
    \label{e:primaldual}
\end{align}

Intuitively, the transformation from \cref{e:ip} to \cref{e:primaldual} can be
explained as follows: rather than trying to minimize error/cost over the
ensemble, we minimize error/cost for each individual tree first (primal
problem). Then we can determine the difference between the number of times it
is used in a single tree and the number of times it is used in the ensemble
($w_{k,i}^{(t)} \leq w_{k,i}$); the dual problem. From the solution to the dual
problem, we can derive new feature costs for every tree (\cref{e:mu} and
\cref{e:primals}) and repeat the same steps until the solutions converge. To
clarify, $\mathcal{T}_{k,i}$ is the set of trees where sample $i$ encounters
feature $k$.

\begin{align}
    \mu_{k,i} &= \frac{\lambda}{N} - \sum_{t \in \mathcal{T}_{k,i}} \beta_{k,i}^{(t)} \label{e:mu} \\
    w_{k,i} &= \begin{cases}
    0 & \text{ if } \mu_{k,i} > 0 \\
    1 & \text{ otherwise}
    \end{cases}
    \label{e:primals}
\end{align}

\cref{e:primaldual} is also subject to constraints mentioned in Section 4 of
Nan et al.~\cite{nan2016pruning} (the algorithm used to solve this problem is
mentioned there as well). Also note the lack of $c_k$ here, too, since the
feature cost in my experiments is always set to $1.0$.
